import '../css/app.css';

import 'phoenix_html';

import React from 'react'
import ReactDom from 'react-dom'

class HelloReact extends React.Component {
  render() {
    return (<h1>Hello World</h1>)
  }
}

ReactDom.render(
  <HelloReact/>,
  document.getElementById('react-app')
);
