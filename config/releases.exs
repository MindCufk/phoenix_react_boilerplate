import Config

config :phoenix_react_boilerplate, PhoenixReactBoilerplateWeb.Endpoint,
       server: true,
       url: [host: System.get_env("APP_NAME") <> ".gigalixirapp.com", port: 443]
